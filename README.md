# Bachelor's thesis

### Student: Lachman František
### Leader: Mgr. Marek Grác, Ph.D. (MU, Red Hat, Inc.)
### Technical leader: Mgr. Tomáš Tomeček (Red Hat, Inc.)

----------


[Building docker images without accessing /build API endpoint](https://diplomky.redhat.com/thesis/show/418/building-docker-images-without-accessing-build-api-endpoint)

----------

The code part of thes thesis was moved to the external repository:

[https://gitlab.com/lachmanfrantisek/incubator](https://gitlab.com/lachmanfrantisek/incubator)

[![build status](https://gitlab.com/lachmanfrantisek/incubator/badges/master/build.svg)](https://gitlab.com/lachmanfrantisek/incubator/commits/master)


