\mdstart

# Appendices

## Tutorial

To be able to try the result of the thesis without any prerequisites, there was created an online tutorial, which shows all the features of the Incubator. The scenario is divided into several steps, where you have a short description and prepared example, that you can try in the terminal emulator besides.

The scenario is hosted at \url{https://www.katacoda.com/incubator/scenarios/tutorial}.

## CLI\label{sec:helpmsg}

\fig{./images/help_message0}{Help message (command)}{helpmsg1}

\fig{./images/help_message1}{Help message (`build` subcommand)}{helpmsg2}

\newpage
\null 
\newpage

## Examples

\fig{./images/example_dockerfile}{Dockerfile used in following examples}{exampledockerfile}

\fig{./images/example}{Example of build output}{example1}

\fig{./images/example_verbose}{Example of build output with verbose option}{example2}

# Sources

- The sources of *Incubator* are hosted in the public repository at~\url{https://gitlab.com/lachmanfrantisek/incubator/} and the~snapshot of the repository is also a digital appendix.
- The sources for the thesis itself (the \LaTeX{} and *Markdown* files as well as the *svg*/*eps* graphics) are hosted in the repository at \url{https://gitlab.com/lachmanfrantisek/bachelor-thesis/} and the snapshot of this repository is present as a digital appendix as well.
- The two patches for the *Dockerfile-parse* can be also found in the digital appendices.


\end{markdown*}
\shorthandon{-}