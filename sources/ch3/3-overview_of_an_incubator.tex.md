\mdstart

# Overview of Incubator

The aim of the thesis is to implement a tool to solve problems from the previous chapter. The tool was named *Incubator*. As well as a newborn, the container image needs good care. Without good care, there is a danger of permanent consequences like published secret keys or slow performance due to bad layering. For this purpose, *Incubator* was invented.

\end{markdown*}
\shorthandon{-}

\input{./sources/ch3/3.1-basic_info.tex.md}
\input{./sources/ch3/3.2-used_technology.tex.md}
\input{./sources/ch3/3.3-git.tex.md}
\input{./sources/ch3/3.4-continues_integration.tex.md}
\input{./sources/ch3/3.5-testing.tex.md}
\input{./sources/ch3/3.6-similar_projects.tex.md}