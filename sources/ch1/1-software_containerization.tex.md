\mdstart

# Software containerization

Software containerization can be seen as the next step of the virtualisation evolution. From virtualisation of hardware, we moved to the level of operating system. There is the same aim -- to make an abstraction layer. In this case, the layer has been moved up.

In the hosting side, there is a kernel. The clients are processes, that are isolated from the host as well as from each other.

This layer simplifies and unifies the view for both sides. There has to be same behaviour for applications on top of the system and the interface for application clients has to stay the same as well.

\end{markdown*}
\shorthandon{-}

\input{./sources/ch1/1.1-overview_of_the_technology.tex.md}
\input{./sources/ch1/1.2-comparison_with_virtualisation.tex.md}
\input{./sources/ch1/1.3-container_as_a_unix_process.tex.md}
\input{./sources/ch1/1.4-docker_introduction.tex.md}
\input{./sources/ch1/1.5-docker_image_build.tex.md}