\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}

\mdstart

*Software containerization* (or *OS-level virtualisation*) is the next generation of server virtualisation \parencite{oslevelvirtualization}. Containers in virtualisation have main properties like the common travel case. They have been developed to transport a wide range of products using different means of transport in the same package. The software ones have the same goal -- to run heterogeneous services in different environments. This method is built on isolating processes and file space instead of running the whole operating system. A kernel of the host system is shared within all containers. Applications inside as well as a user has a feeling of using the real system. The first chapter of the thesis aims to describe more precisely the software containerization and its differences with the common whole-system virtualisation.

The object of this thesis is to develop an application for building *docker images* \parencite{thesis}. *Container image* is a static entity containing data and operational information. Container comes into being when the image is started. Docker images and containers are backed by Docker Inc. This company's container engine already has a component *builder* used to create new containers. There are some important issues, which Docker Inc. is not planning to solve. With the original builder, there is no way to use secrets during build time or to precisely control layering and caching. Also, metadata of the final image needs to be handled more accurately. Alternative builder (developed as a result of this thesis) is called *Incubator* and aims to solve these problems, which will be deeply described in the second chapter.

Docker engine is a *client-server* service. Communication is done through docker engine's *API* (Application Interface). As well as the build functionality, API contains low-level methods for manipulating with images and containers. These were made by *upstream*. With the usage of API calls for creating, starting, and committing containers is alternative builder able to create images like the company's one. In addition, the newly implemented builder gives us more power to control the build process.

A general overview of an application structure will be described in the chapter number three. *Incubator* was written in Python programming language \parencite{python} (compatible with both version 2 and 3) and uses two libraries -- *Docker-py*[^1] for communication with docker API and *Dockerfile-parse*[^2] for processing so-called *Dockerfiles*. These files are used to describe the process of the image build. They contain information about parent image (standard images always extend existing ones), actions to do during the build and some metadata specification -- exposed ports, environment variables, and another setup.

In chapter number four, there will be described solutions for particular problems. There will be described details about implementing build-time volumes, layering and metadata handling.

The source of the *Incubator* project is licensed under the \href{https://mit-license.org/}{*MIT license*} \parencite{licensemit}, whereas the thesis itself uses a *\doclicenseLongNameRef* license \parencite{licensecc}.

The thesis was assigned by Red Hat, Inc. \parencite{thesis}. The company aims to integrate the builder with current tools used to deploy its products. Future of this builder will be mentioned in the last chapter of the thesis.


[^1]: Docker-py \parencite{dockerpy} is a *Docker Software Development Kit* for Python and will be described in the subsection \ref{ssec:dockerpy}.

[^2]: Dockerfile-parse \parencite{dockerfileparse} is a Python library for parsing *Dockerfiles* (will be mentioned in the subsection \ref{ssec:dockerfileparse}).

\end{markdown*}
\shorthandon{-}
