\mdstart

# Future

The thesis itself is not the target of the project. Its aim is to create a~new open-source project. The future will be driven by the user demand, collaboration of developers and changes in the containerisation ecosystem in general. The prospects for the future will be discussed in the following chapter.

## Connection to other systems

The tool itself will have no results without some usage. The near future will be focused on broadening *Incubator* and try as much build processes as possible. This wider testing will probably bring new requirements, exposed errors and contributors.

After testing by the community, the tool will be prepared to the use in bigger projects. Since the development has been doing with the respect to replace-ability, the test of building production images will not be hard.

## Container clients\index{Docker}\index{*rkt*}\index{containerization}

Apart from the Docker engine, there are other container systems such as *rkt* \parencite{rkt}. Also, the docker engine is being split to the smaller parts which are replaceable and are being standardised. For Incubator in the future, it would be important to be separated from the particular build or runtime engine and allow users to choose the needed modules on their own.

The container functionality is now in the separated class and allows to be easily replaced by another system. The possible approach will be to use some plug-in system, that allows swapping the engine and/or provide some OCI\index{OCI} compatible API in build (allowing the build process itself) and run side (the format of created image).

## Caching\index{caching}

The biggest problem with *Incubator* in the current state is the missing ability to cache already created images. *Incubator* itself is for the implementation prepared. Before the new layer is created, *Incubator* asks an `ImageCache` instance if the image already exists. It can be resolved with the knowledge of parent image and the current layer instructions. The cache object returns the id of a cached image or nothing.

After the commit for the container, this information (parent image and instructions on the last layer) is saved to the cache. Nowadays, the cache does nothing -- to existence request, it always returns nothing and saves also nothing. The only job of the current cache implementation is to print some debug data.

This interface is now given, but the implementation can be done in many ways. There has to be saved images and the build graph representing parent hierarchy and applied instructions.

The cache can use the native docker imaging system or choose its own organisation. The images can also be saved as a directory structure with archives for the layers. Also, there can be used a database for the structure and the images can be saved externally. One can imagine many ways to do this, but it depends on the particular use-case. Ideally, *Incubator* will provide more types of cache to allow a user to choose the right one, which fits the best.

## Current problems\index{*Dockerfile-parse*}

*Incubator* is built on top of other libraries which brings some problems. When working on the changes for the *Dockerfile-parse*, there was discovered, that the parsing of environment variables is not always valid. Using such complicated variables or labels is not common, but there was submitted an issue \parencite{dockerfileparseissue}. If the project maintainers or someone else does not implement the right parsing, it will be necessary to do it.

There are also some rare instruction values, which was not implemented yet (e.g. remote source for `ADD`).

The big problem is the instability of the *Docker-py*.\index{*Docker-py*} There are some methods, which does not behave like they have to. Some problems are due to the *Request* library used by *Docker-py*, which causes some connection failures -- e.g. sending data in `ADD`/`COPY` has to be repeated, when there is a bigger amount of files (hundreds of kilobytes is enough), the connection is refused.

There are also some other smaller inconsistencies with the behaviour, which has needed some alternative solutions. For the future, it would be convenient to report all these problems and help to solve them. It will make *Incubator* more stable and robust.

\end{markdown*}
\shorthandon{-}
