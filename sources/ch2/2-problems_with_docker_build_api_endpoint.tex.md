\mdstart

# Problems with the docker `/build` API endpoint\index{docker builder}

The containerization engine Docker provides a way to create a new image with a component called *builder*. It is available at `/build` endpoint of the API. In this chapter, there will be a description of three main problems with this component. The solutions will be discussed in section \ref{sec:solutions}.

\end{markdown*}
\shorthandon{-}

\input{./sources/ch2/2.1-layering.tex.md}
\input{./sources/ch2/2.2-secrets.tex.md}
\input{./sources/ch2/2.3-metadata.tex.md}