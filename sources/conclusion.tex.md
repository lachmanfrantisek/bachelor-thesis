\chapter*{Conclusion}
\addcontentsline{toc}{chapter}{Conclusion}

\mdstart

The idea of the thesis was born in the action *Red Hat Open House 2016* and after more than a year of the planning, learning and coding, it achieved its target. The tool able to create a docker image without accessing `/build` API endpoint was created.

The development of *Incubator* also reflects the evolution of the Docker project itself in the last year. The Summer was spent in the name of finding solutions for burning problems. The open-source community was trying to work out the problem with expanding of the engine and *Incubator* was finding the way to precise layering, storing of secrets and handling the metadata. The Winter brings breaking of the API and instability of the used technology. Docker changes the version schema, *Docker-py* was remade and *Incubator* was trying to get the base functionality on top of this unstable ground.

The Spring comes with the expectations of the collaboration and awaiting the *Moby project*. The development of *Incubator* was concentrated in the collaboration on the *Dockerfile-parse*.

The Summer yielded the crops and *Incubator* solved the desired problems. Everyone can look forward to the first harvest, but the future crops will be definitely more sweat.

\end{markdown*}
\shorthandon{-}