\mdstart

# Implementation

The implementation of *Incubator* started in the August 2016 and the basic structure and working build were done in the Autumn. During Spring, there has been work on particular instructions and on a very time-consuming implementation of the changes to *Dockerfile-parse*.

The submitting of the thesis will not stop working on the project. Like with other tools, the development is never done. *Incubator* needs to keep pace with docker builder as well as with the changes in *Docker-py*. Since these two projects are always evolving, *Incubator* should not stop the development too.

Also, there can occur new use-cases and requirements. The wider usage will probably find new bugs, that will need correction.

The following chapter will take a deeper look into the implementation of some parts of *Incubator*.


\end{markdown*}
\shorthandon{-}

\input{./sources/ch4/4.1-build_workflow.tex.md}
\input{./sources/ch4/4.2-configuration.tex.md}
\input{./sources/ch4/4.3-solving_problems.tex.md}
\input{./sources/ch4/4.4-implementation_of_dockerfile_instructions.tex.md}
\input{./sources/ch4/4.5-cli.tex.md}
\input{./sources/ch4/4.6-output.tex.md}